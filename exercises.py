def is_anagram(s1, s2):
    """
    Write an algorithm that returns whether s1 and s2 are anagrams of each other, i.e.
    if s1 and s2 contain the same letters in a possibly different order.
    E.g.: "abc" and "cab" are anagrams.
    :param s1: string
    :param s2: string
    :return: True or False
    """
    # convert string inputs as list
    l_s1 = list(s1)
    l_s2 = list(s2)
    # use built-in in-place sort function
    l_s1.sort()
    l_s2.sort()
    return l_s1 == l_s2


def check_parenthesis_consistency_only(string):
    """
    Write an algorithm that determines if the parentheses (round brackets "()") in a string are properly balanced.
    An expression is said to be properly parenthesised if it has the form "(p)" or "pq", where p and q are
    properly parenthesised expressions. Any string (including an empty string) that does not contain any parenthesis
    is properly parenthesised.
    E.g.: "()()" is properly parenthesised, "(()" is not.
    :param string: the string to analyse.
    :return: True if the parentheses are balanced, False if not.
    """

    l_string = list(string)

    # immediatly return True if string is empty
    if l_string == []:
        return True
    else:
        l_stack = []
        for i in l_string:
            # pass to next if character is not a parenthesis
            if i != "(" and i != ")":
                pass
            # process otherwise
            else:
                if i == "(":
                    l_stack.append("(")
                elif len(l_stack) > 0:
                    l_stack.pop()
                else:
                    return False
        if len(l_stack) == 0:
            return True
    return False


def check_any_bracket_consistency(string):
    """
    Write an algorithm that determines if the round brackets "()", square brackets "[]" and curly braces "{}" in a
    string are properly balanced.
    E.g.: "(){}" is OK, "({)}" is not.
    :param string: the string to analyse.
    :return: True if the brackets are balanced, False if not.
    """

    l_string = list(string)
    l_open_parenthesis = ["[", "{", "("]
    l_close_parenthesis = ["]", "}", ")"]

    # immediatly return True if string is empty
    if l_string == []:
        return True
    else:
        l_stack = []
        for i in l_string:
            # pass to next if character is not a bracket
            if i not in (l_open_parenthesis + l_close_parenthesis):
                pass
            # process otherwise
            else:
                if i in l_open_parenthesis:
                    l_stack.append(i)
                elif (
                    len(l_stack) > 0
                    and l_stack[-1] == l_open_parenthesis[l_close_parenthesis.index(i)]
                ):
                    l_stack.pop()
                else:
                    return False
        if len(l_stack) == 0:
            return True
    return False
